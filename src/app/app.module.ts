import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { AdminComponent } from "./components/admin/admin.component";
import { MaterialModule } from "./material/material.module";
import { CountUpModule } from "ngx-countup";

/* Services */
import { PostsService } from "./services/posts/posts.service";
import { AlbumsService } from "./services/albums/albums.service";
import { UsersService } from "./services/users/users.service";
import { TodosService } from "./services/todos/todos.service";

/* Dialogs */
import { DeleteDialogComponent } from "./components/admin/dialogs/delete-dialog/delete-dialog.component";
import { EditDialogComponent } from "./components/admin/dialogs/edit-dialog/edit-dialog.component";
import { ViewDialogComponent } from "./components/admin/dialogs/view-dialog/view-dialog.component";
import { ProfileCardComponent } from "./components/admin/dialogs/view-dialog/profile-card/profile-card.component";
import { PostsCardComponent } from "./components/admin/dialogs/view-dialog/posts-card/posts-card.component";
import { AlbumsCardComponent } from "./components/admin/dialogs/view-dialog/albums-card/albums-card.component";
import { TodosCardComponent } from "./components/admin/dialogs/view-dialog/todos-card/todos-card.component";
import { NumberCardComponent } from "./components/dashboard/number-card/number-card.component";
import { GraphCardComponent } from "./components/dashboard/graph-card/graph-card.component";

const routes: Routes = [
  { path: "dashboard", component: DashboardComponent },
  { path: "admin", component: AdminComponent },
  {
    path: "",
    redirectTo: "/dashboard",
    pathMatch: "full"
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AdminComponent,
    DeleteDialogComponent,
    EditDialogComponent,
    ViewDialogComponent,
    ProfileCardComponent,
    PostsCardComponent,
    AlbumsCardComponent,
    TodosCardComponent,
    NumberCardComponent,
    GraphCardComponent
  ],
  entryComponents: [
    DeleteDialogComponent,
    EditDialogComponent,
    ViewDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CountUpModule,
    ChartsModule ,
    RouterModule.forRoot(routes, { enableTracing: false })
  ],
  providers: [PostsService, AlbumsService, UsersService, TodosService],
  bootstrap: [AppComponent]
})
export class AppModule {}
