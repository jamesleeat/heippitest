import { Component, OnInit, ViewChild } from "@angular/core";
import { SelectionModel } from "@angular/cdk/collections";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from './dialogs/delete-dialog/delete-dialog.component';
import { EditDialogComponent } from './dialogs/edit-dialog/edit-dialog.component';
import { ViewDialogComponent } from './dialogs/view-dialog/view-dialog.component';



export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  phone: string;
}

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"]
})

/* Componente que muestra una lista con usuarios. Se pueden ver, editar, o eliminar uno o varios usuarios  */
export class AdminComponent implements OnInit {

  displayedColumns: string[];                                 // Lista de columnas que seran mostradas
  dataSource: any;                                            // Fuente de informacion para la tabla de material
  users: any[];                                               // Lista de usuarios
  selection = new SelectionModel<User>(true, []);             // Seleccion de usuario(s)
  @ViewChild(MatSort, { static: true }) sort: MatSort;        // Componente de organizacion ascendente o descendente de material

  /* Se inyecta la dependencia de los dialogos para poder acceder a su referencia */
  constructor(public dialog: MatDialog) {}

  ngOnInit() {

    this.users = JSON.parse(localStorage.getItem("users"));           // Lista de usuarios se inicializan con el valor guardado en localstorage
    this.dataSource = new MatTableDataSource<User>(this.users);       // La fuente de informacion se inicializa
                                                                      // con una fuente de informacion de material que se le pasa la lista de usuarios

    this.dataSource.sort = this.sort;                                 // Propiedad para asignar el componente de sorteo
    /*Lista de columnas que seran mostradas */
    this.displayedColumns = [
      "select",
      "id",
      "name",
      "username",
      "email",
      "phone",
      "options"

    ];
  }
  /** Verificacion de la seleccion total o no de los elementos de la tabla*/
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selecciona todas las columnas o limpia todas las selecciones */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** Etiqueta para las casillas que esten seleccionadas */
  checkboxLabel(row?: User): string {
    if (!row) {
      return `${this.isAllSelected() ? "select" : "deselect"} all`;
    }
    return `${
      this.selection.isSelected(row) ? "deselect" : "select"
    } row ${row.id + 1}`;
  }

  /* Dialogo (modal) para confirmar la eliminacion de un usuario de la lista */
  deleteDialog() {
    let dialogRef = this.dialog.open(DeleteDialogComponent );

    dialogRef.afterClosed().subscribe(result => {

      /* Si el usuario confirma, se elimina la seleccion hecha por el mismo. */
      if(result === 'true') {
        let deletedUsersId = this.selection.selected.map(user  => user.id);
        this.deleteUsers(deletedUsersId);
      }
    })
  }

  /* Dialogo (modal)  para editar la informacion basica de un usuario*/
  editDialog(user) {
    let dialogRef = this.dialog.open(EditDialogComponent, {
      width: '400px',
      height: '450px',
      data: {
        name: user.name,
        username: user.username,
        email: user.email,
        phone: user.phone
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        result.id = user.id
        this.editUser(result);
      }
    })
  }

  /* Dialogo (modal) que muestra informacion detallada del usuario */
  viewDialog(user) {
      this.dialog.open(ViewDialogComponent, {
      width: '400px',
      height: '600px',
      data: {
        id: user.id
      }
    });
  }

  /* Funcion para eliminar a los usuarios seleccionados de memoria */
  deleteUsers(deletedUsers){
    this.users = this.users.filter( user => !deletedUsers.includes(user.id));  // Se filtran los usuarios que seran eliminados
    localStorage.setItem('users', JSON.stringify(this.users));                 // Se asigna la nueva lista de usuarios al localstorage
    this.dataSource = new MatTableDataSource<User>(this.users);                // Se actualiza la tabla con los nuevos usuarios
  }

  /* Funcion para editar al usuario seleccionado */
  editUser(data) {
    this.users[this.users.findIndex(us => us.id === data.id)] = data;         // Se reemplaza la referencia local del usuario por la nueva informacion
    localStorage.setItem('users', JSON.stringify(this.users));                // Se actualiza la referencia en localstorage
    this.dataSource = new MatTableDataSource<User>(this.users);               // Se actualiza la lista con la nueva informacion
  }
}
