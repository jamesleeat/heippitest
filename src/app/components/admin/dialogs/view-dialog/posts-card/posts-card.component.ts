import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-posts-card',
  templateUrl: './posts-card.component.html',
  styleUrls: ['./posts-card.component.scss']
})

/* Componente para mostrar la lista de posts del usuario */
export class PostsCardComponent implements OnInit {

  @Input() posts;
  constructor() { }

  ngOnInit(): void {
  }

}
