import { Component, OnInit, Inject } from "@angular/core";
import { UsersService } from "src/app/services/users/users.service";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PostsService } from "src/app/services/posts/posts.service";
import { AlbumsService } from "src/app/services/albums/albums.service";
import { TodosService } from "src/app/services/todos/todos.service";

@Component({
  selector: "app-view-dialog",
  templateUrl: "./view-dialog.component.html",
  styleUrls: ["./view-dialog.component.scss"]
})

/* Dialogo que muestra la informacion detallada del usuario */
export class ViewDialogComponent implements OnInit {
  user: any; // Usuario a visualizar
  posts: any; // Posts del usuario
  postsNumber: any;
  albumsNumber: any;
  todosNumber: any;
  albums: any; // Albums del usuario
  todos: any; // Tareas (todos) del usuario
  loading: boolean; // Variable para determinar si se estan o no cargando datos

  /* Para poder acceder a la informacion que se pasa en el componente padre, es necesario injectar el componente MAT_DIALOG_DATA */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UsersService,
    private postsService: PostsService,
    private albumsService: AlbumsService,
    private todosService: TodosService
  ) {}

  ngOnInit(): void {
    this.loading = true; // Solo si ya se tienen todos los datos, se debe dejar de cargar.

    /* Se obtiene la informacion necesitada del usuario para popular las tarjetas de informacion. */

    this.userService.getUser(this.data.id).subscribe(
      user => {
        this.user = user;
      },
      error => {
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );

    this.postsService.getPostsPerUser(this.data.id).subscribe(
      posts => {
        this.posts = posts;
      }
    );

    this.albumsService.getUserAlbums(this.data.id).subscribe(
      albums => {
        this.albums = albums;
      }
    );

    this.todosService.getTodosPerUser(this.data.id).subscribe(
      todos => {
        this.todos = todos;
      }
    );
  }
}
