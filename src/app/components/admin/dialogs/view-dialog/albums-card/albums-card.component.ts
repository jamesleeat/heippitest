import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-albums-card',
  templateUrl: './albums-card.component.html',
  styleUrls: ['./albums-card.component.scss']
})

/* Componente para mostrar la lista de albums del usuario */
export class AlbumsCardComponent implements OnInit {

  @Input() albums;
  constructor() { }

  ngOnInit(): void {
  }

}
