import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss']
})

/* Componente para mostrar la informacion personal del usuario */
export class ProfileCardComponent implements OnInit {
  @Input() user: any;
  @Input() posts: any;
  @Input() albums: any;
  @Input() todos: any;
  constructor() { }

  ngOnInit(): void {
  }

}
