import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-todos-card',
  templateUrl: './todos-card.component.html',
  styleUrls: ['./todos-card.component.scss']
})

/* Componente para mostrar la lista de tareas (todos) del usuario */
export class TodosCardComponent implements OnInit {

  @Input() todos;
  constructor() { }

  ngOnInit(): void {
  }

}
