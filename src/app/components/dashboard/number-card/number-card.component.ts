import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-number-card',
  templateUrl: './number-card.component.html',
  styleUrls: ['./number-card.component.scss']
})

/* Componente que muestra un conteo animado */
export class NumberCardComponent implements OnInit {

  @Input() number;            // Numero para mostrar
  @Input() name;              // Nombre de la persona asociada con la categoria
  constructor() { }

  ngOnInit(): void {
  }

}
