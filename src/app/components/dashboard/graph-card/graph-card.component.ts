import { Component, OnInit, Input } from "@angular/core";
import { ChartOptions, ChartType, ChartDataSets } from "chart.js";
import { Label } from "ng2-charts";

@Component({
  selector: "app-graph-card",
  templateUrl: "./graph-card.component.html",
  styleUrls: ["./graph-card.component.scss"]
})

/* Componente usado para mostrar una simple grafica de barras usando la libreria ng2-charts con chart.js */
export class GraphCardComponent implements OnInit {

  @Input() data: any[];                                 // Info que sera mostrada
  @Input() label: string;                               // Etiqueta que nombra a la informacion

  /* Opciones de el grafico de barras. */
  public barChartOptions: ChartOptions = {
    responsive: true,                                   // Para poder tener un grafico responsivo ante los cambios de resolicion
    maintainAspectRatio: true,                          // Mantiene la relacion de pixeles
  };


  public barChartLabels: Label[];                       // Etiquetas a mostrar en el eje X. Normalmente, seran los nombres de los usuarios
  public barChartType: ChartType = "bar";               // Tipo de grafico que se desea (barra)

  public barChartData: ChartDataSets[];                 // Informacion para llenar los graficos

  constructor() {
  }

  ngOnInit(): void {

    /*
        Se inicializan las etiquetas y la informacion de las graficas
        haciendo un simple mapeo de cada usuario con cada dato que le corresponde
    */
    this.barChartLabels = this.data.map( (data) => data.name );
    this.barChartData = [
      { data: this.data.map( (data) => data.info), label: this.label }
    ];
  }
}
