import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})

/* Panel informativo con estadisticas y graficos de los usuarios, posts, albums, y todos. */
export class DashboardComponent implements OnInit {


  users: any;                                       // Lista de usuarios
  posts: any;                                       // Lista de publicaciones (posts)
  todos: any;                                       // Lista de tareas (Todos)
  albums: any;                                      // Lista de albums (No son imagenes)

  postsPerUserId: any;                              // Numero de posts por id de usuario
  todosPerUserId: any;                              // Numero de tareas (Todos) por id de usuario
  albumsPerUserId: any;                             // Numero de albumes por id de usuario

  topPostingUser: any;                               // Usuario con el mayor numero de publicaciones
  topTodoUser: any;                                  // Usuario con el mayor numero de tareas (todos) completadas
  postsDataSource: any;                              // Datos usuarios y sus posts para graficar
  todosDataSource: any;                              // Datos de usuarios y todos completados para graficar
  albumsDataSource: any;                             // Datos de usuarios y sus albumes para graficar
  loading: boolean;                                  // Variable para determinar el estado de carga y mostrar un spinner o no

  constructor() {}

  ngOnInit(): void {

    /* Se toman los datos del localStorage y se inicializan las variables respectivas */
    this.users = JSON.parse(localStorage.getItem("users"));
    this.posts = JSON.parse(localStorage.getItem("posts"));
    this.todos = JSON.parse(localStorage.getItem("todos"));
    this.albums = JSON.parse(localStorage.getItem("todos"));

    /*Inicializada en true para indicar la carga de datos*/
    this.loading = true;

    /* Obtenemos una lista de id de usuarios con la cantidad de posts y albumes asociadas a ellos */
    this.postsPerUserId = this.getNumbers(this.posts);
    this.albumsPerUserId = this.getNumbers(this.albums);

    /* Antes de obtener el numero de todos, debemos filtrar aquellos que esten completados*/
    this.todosPerUserId = this.todos.filter((todo: any) => {
      return todo.completed === true;
    });

    /* Obtenemos una lista id de usuarios con los todos asociados*/
    this.todosPerUserId = this.getNumbers(this.todosPerUserId);

    /*
        Primero se obtiene el mayor numero de posts o todos asociados con un id.
        Luego, usamos ese id para encontrar la informacion detallada del usuario en cuestion.
    */
    this.topPostingUser = this.getHighestNumber(this.postsPerUserId);
    this.topPostingUser = this.users.find(user => user.id == this.topPostingUser);

    this.topTodoUser = this.getHighestNumber(this.todosPerUserId);
    this.topTodoUser = this.users.find(user => user.id == this.topTodoUser);


    /*
      Para graficar, requerimos hacer un mapeo del nombre del usuario y
      la cantidad de posts, todos, o albumes asociados a este.
    */
    this.postsDataSource = this.users.map(user => {
      return {
        name: user.name,
        info: this.postsPerUserId[user.id]
      };
    });
    this.todosDataSource = this.users.map(user => {
      return {
        name: user.name,
        info: this.todosPerUserId[user.id]
      };
    });
    this.albumsDataSource = this.users.map(user => {
      return {
        name: user.name,
        info: this.albumsPerUserId[user.id]
      };
    });

    /* Este timeout es para retrasar un poco la carga de los datos y mostrar el spinner por un segundo.*/
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  /*
    Recibe un objeto como parametro y retorna un objeto de objetos
    que contiene el id del usuario y la cantidad repetida de info requerida.
  */
  getNumbers(obj: any) {
    return obj.reduce((objects, object) => {
      objects[object.userId] = ++objects[object.userId] || 1;
      return objects;
    }, {});
  }

  /*
    Recibe un objeto con la cantidad de posts o todos repetidos para encontrar
    la id asociada con la mayor cantidad de repeticiones.
  */
  getHighestNumber(objNumber: any) {
    return Object.keys(objNumber).reduce((a, b) =>
      objNumber[a] >= objNumber[b] ? a : b
    );
  }
}
