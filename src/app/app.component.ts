import { Component } from "@angular/core";
import { UsersService } from "./services/users/users.service";
import { TodosService } from "./services/todos/todos.service";
import { AlbumsService } from "./services/albums/albums.service";
import { PostsService } from "./services/posts/posts.service";
/* TODO
    Agregar State management (ngrx)
    Agregar unitTests
*/

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "User Forum";
  constructor(
    private usersService: UsersService,
    private todosService: TodosService,
    private postsService: PostsService,
    private albumsService: AlbumsService
  ) {
    /* Informacion es obtenida y almacenada en local storage para ser posteriormente manipulada */

    this.usersService.getUsers().subscribe((users: any) => {
      users.forEach((user: any) => {
        /* Estos campos no son relevantes para la lista de usuarios inicial */
        delete user.address;
        delete user.company;
        delete user.website;
      });
      localStorage.setItem("users", JSON.stringify(users));
    });

    this.postsService.getPosts().subscribe((posts): any => {
      localStorage.setItem("posts", JSON.stringify(posts));
    });

    this.todosService.getTodos().subscribe((todos): any => {
      localStorage.setItem("todos", JSON.stringify(todos));
    });

    this.albumsService.getAlbums().subscribe((albums): any => {
      localStorage.setItem("albums", JSON.stringify(albums));
    });
  }
}
