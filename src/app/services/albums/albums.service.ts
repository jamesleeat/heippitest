import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'
@Injectable({
  providedIn: 'root'
})

/* Servicio relacionado con los albumes de cada usuario. Ninguno de estos endpoints retornan imagenes o links a alguna */
export class AlbumsService {

  constructor(private http: HttpClient) { }

  /* Obtiene una lista con los albumes de todos los usuarios */
  getAlbums(){
    return this.http.get(`${environment.URL_API}/albums`);
  }

  /* Obtiene los albums de un usuario usando su ID */
  getUserAlbums(id: number){
    return this.http.get(`${environment.URL_API}/albums?userId=${id}`);
  }
}
