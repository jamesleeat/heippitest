import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})

/* Obtiene informacion concerniente a los usuarios. */
export class UsersService {

  constructor(private http: HttpClient) { }

  /* Obtiene una lista de usuarios*/
  getUsers() {
    return this.http.get(`${environment.URL_API}/users`);
  }

  /* Obtiene un usuario por su ID*/
  getUser(id: number) {
    return this.http.get(`${environment.URL_API}/users/${id}`);
  }
}
