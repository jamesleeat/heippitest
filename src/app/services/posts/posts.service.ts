import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'
@Injectable({
  providedIn: 'root'
})

/* Servicios relacionados con las publicaciones (Posts) de los usuarios */
export class PostsService {

  constructor(private http: HttpClient) { }

  /* Obtiene una lista de todos los posts de los usuarios*/
  getPosts() {
    return this.http.get(`${environment.URL_API}/posts`);
  }


  /* Obtiene todos los posts de un usuario por su ID */
  getPostsPerUser(id: number) {
    return this.http.get(`${environment.URL_API}/posts?userId=${id}`);
  }
}
