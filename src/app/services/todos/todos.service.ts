import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: "root"
})

/* Servicios relacionados con las tareas completadas o no de los usuarios */
export class TodosService {
  constructor(private http: HttpClient) {}

  /* Obtiene LA lista de tareas (Todos)  de todos los usuarios*/
  getTodos() {
    return this.http.get(`${environment.URL_API}/todos`);
  }

  /* Obtiene la lista de tareas de un solo usuario con su ID */
  getTodosPerUser(id: number) {
    return this.http.get(`${environment.URL_API}/todos?userId=${id}`);
  }
}
